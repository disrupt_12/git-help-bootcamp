# ERRORES EN GIT

## A) Failed to push some refs

Al intentar realizar un **push** a [GitLab](https://gitlab.com/)  desde la *Terminal*, como el siguiente:
 **`$git push -u git@gitlab.com:nombre-repositorio.git master`**

Nos aparece el siguiente error:

**`$ error: src refspec master does not match any.
error: failed to push some refs to 'git@gitlab.com:disrupt_12/git-help-bootcamp.git'`**

## Posibles soluciones

>Motivo1: no hemos hecho un **commit** de los cambios.

**Solución 1**:
> Añadir los ficheros que nos interesa **subir** al *repositorio.*
> 1. **`$ git add .`**  Este comando añade **todos los ficheros,**
> o bien podemos seleccionar los ficheros a añadir **`$git add nombre-fichero1 nombre-fichero2 ...`**
>2. **`$ git commit -m "Mensaje para el commit"`**
>3. **`$git push -u git@gitlab.com:nombre-repositorio.git master`**
#
>Motivo2: 

**Solución 2**:
> 