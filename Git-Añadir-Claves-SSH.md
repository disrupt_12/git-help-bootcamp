# Claves SSH

Antes de comenzar:
> 1. Abrir una **terminal**
> 2. **Log in** a vuestro perfil de [GitLab](https://gitlab.com/)

**¿Como generar las claves SSH?**
1. Abrir la *Terminal*
2. Ingresar el siguiente *comando* con **vuestra dirección de correo del bootcamp**: **`$ ssh-keygen -o -t rsa -b 4096 -C direccionCorreoBootcamp.com`**
3. La *Terminal* nos va a preguntar: 
	  **`$ Enter file in which to save the key (/Users/sebas/.ssh/id_rsa):`**
4. Presionamos *Enter*
	>Si ya existe una clave, preguntará si queremos sobreescribir.
	
	**`$ /Users/sebas/.ssh/id_rsa already exists. Overwrite (y/n)?`**
	
	Escribimos `y` + *Enter* para confirmar
5. Ingresamos una **passphrase o contraseña**
	>Si no queremos ninguna, presionamos Enter
	
	**`$ Enter passphrase (empty for no passphrase)`**
7. Ingresar de nuevo la **contraseña** para confirmar.
8. La terminal imprime un mensaje de creación de la **clave SSH pública, llamada (id_rsa.pub).**
9. Podemos comprobar la creación de la clave **ejecutando:**
	`$  cat ~/.ssh/id_rsa.pub`
	>Este comando muestra por pantalla la cadena de caracteres que componen nuestra clave SSH pública.


# Añadir Clave SSH a GitLab

1. Necesitamos **copiar la clave SSH** publica que hemos generado 
2. **Ejecutar** en la *terminal*:
	 `$  cat ~/.ssh/id_rsa.pub`
3. Seleccionar **toda la cadena impresa** y copiarla *(Ctrl + C)*
4. Abrimos   [GitLab](https://gitlab.com/)
5. *Click* en nuestro **Perfil** (esquina superior derecha)
6. *Click* **Settings**
7. *Click* **SSH Keys**
8. Pegamos en el **cuadro de texto** la cadena de la clave SSH que hemos copiado desde la *Terminal*
9. *Click* al botón **"Add Key"**  

